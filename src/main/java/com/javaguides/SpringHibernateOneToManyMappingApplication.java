package com.javaguides;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.javaguides.entity.Contrato;
import com.javaguides.entity.ContratoAditivo;
import com.javaguides.repository.ContratoRepo;

@SpringBootApplication
public class SpringHibernateOneToManyMappingApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringHibernateOneToManyMappingApplication.class, args);
	}

	
	@Autowired
	private ContratoRepo contratoRepo;
	
	
	
	public void insertNovosContratos() {
		Contrato contrato = new Contrato("Contrato 001");
		
		ContratoAditivo contratoAditivo1 = new ContratoAditivo("Aditivo 01 do contrato 001");
		ContratoAditivo contratoAditivo2 = new ContratoAditivo("Aditivo 02 do contrato 001");
		ContratoAditivo contratoAditivo3 = new ContratoAditivo("Aditivo 03 do contrato 001");
		
		contrato.getListaAditivos().add(contratoAditivo1);
		contrato.getListaAditivos().add(contratoAditivo2);
		contrato.getListaAditivos().add(contratoAditivo3);
		
		this.contratoRepo.save(contrato);
	}
	
	public void listarTodosContratos() {
		List<Contrato> contratosSalvos = this.contratoRepo.findAll();
		
		System.out.println(contratosSalvos);
	}
	
	
	
	public void pesquisarPorId(Integer seqContrato) {
		Contrato contratoFound = contratoRepo.pesquisarContratoPorId(seqContrato);
		System.out.println("contrato achado  " + contratoFound);
	}
	
	public void findById() {
		Optional<Contrato> contratoProcurado = contratoRepo.findById(1);
		Contrato contratoAchado = new Contrato();
		
		if(contratoProcurado.isPresent()) {
			contratoAchado = contratoProcurado.get();
			
			List<ContratoAditivo> listaAditivo =  contratoAchado.getListaAditivos();
			ContratoAditivo aditivo1 = listaAditivo.get(0);
			aditivo1.setDscContratoAditivo("Descrição Aditivo 01 do Contrato 01 editado");
			
			listaAditivo.set(0, aditivo1);
			
		}
		contratoRepo.save(contratoAchado);
		
		
	}
	
	public void updateAditivosDeUmContrato() {
		List<Integer> ids = new ArrayList();
		ids.add(1);// contrato 001
		
		List<Contrato> contratoFound = this.contratoRepo.findAllById(ids);
		System.out.println("contrado achado " + contratoFound +  "\r\n");
		
		List<ContratoAditivo> aditivos =  contratoFound.get(0).getListaAditivos();
		
		System.out.println(" Aditivos do Contrato" + aditivos);
		
		for (int i=0 ; i < aditivos.size() ; i++) {
			if( aditivos.get(i).getSeqContratoAditivo() == 1 ) { //pesquisa pelo aditivo numero 01
				
				ContratoAditivo aditivoToEdit = aditivos.get(i);
				aditivoToEdit.setDscContratoAditivo("aditivo 01 do contrato 001, editado");
				
				aditivos.set(i, aditivoToEdit);
			}
		}
		
		contratoFound.get(0).setListaAditivos(aditivos);
		
		this.contratoRepo.save(contratoFound.get(0));
		
	}
	
	
	
	@Override
	public void run(String... args) throws Exception {
		
		//insertNovosContratos();
		
		//listarTodosContratos();
		
	//	updateAditivosDeUmContrato();
		
		//findById();
		
		pesquisarPorId(1);
		
		
		
		
	}

}
