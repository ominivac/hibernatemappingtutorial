package com.javaguides.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "aditivo")
public class ContratoAditivo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer seqContratoAditivo;
	
	@Column(name = "dsc_aditivo")
	private String dscContratoAditivo;
	
	
	public ContratoAditivo() {
		
	}
	
	
	public ContratoAditivo(String dscContratoAditivo) {
		super();
		this.dscContratoAditivo = dscContratoAditivo;
	}
	public Integer getSeqContratoAditivo() {
		return seqContratoAditivo;
	}
	public void setSeqContratoAditivo(Integer seqContratoAditivo) {
		this.seqContratoAditivo = seqContratoAditivo;
	}
	public String getDscContratoAditivo() {
		return dscContratoAditivo;
	}
	public void setDscContratoAditivo(String dscContratoAditivo) {
		this.dscContratoAditivo = dscContratoAditivo;
	}


	@Override
	public String toString() {
		return "seqContratoAditivo=" + seqContratoAditivo + ", dscContratoAditivo="+ dscContratoAditivo + " \r\n";
	}
	
	
	

}
