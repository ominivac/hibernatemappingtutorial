package com.javaguides.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "contrato")
public class Contrato {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer seqContrato;
	
	@Column(name = "dsc_contrato")
	private String dscContrato;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "contrato_seq", referencedColumnName = "seqContrato")
	private List<ContratoAditivo> listaAditivos = new ArrayList<>();

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "contrato_categoria", joinColumns = @JoinColumn(name = "seqContrato"), 
			  inverseJoinColumns = @JoinColumn(name = "seqCategoriaContrato"))
	private Set<CategoriaContrato> categorias;
	
	public Contrato() {
		
	}
	
	
	public Contrato(String dscContrato) {
		super();
		this.dscContrato = dscContrato;
	}

	public Integer getSeqContrato() {
		return seqContrato;
	}

	public void setSeqContrato(Integer seqContrato) {
		this.seqContrato = seqContrato;
	}

	public String getDscContrato() {
		return dscContrato;
	}

	public void setDscContrato(String dscContrato) {
		this.dscContrato = dscContrato;
	}

	public List<ContratoAditivo> getListaAditivos() {
		return listaAditivos;
	}

	public void setListaAditivos(List<ContratoAditivo> listaAditivos) {
		this.listaAditivos = listaAditivos;
	}

	
	

	public Set<CategoriaContrato> getCategorias() {
		return categorias;
	}


	public void setCategorias(Set<CategoriaContrato> categorias) {
		this.categorias = categorias;
	}


	@Override
	public String toString() {
		return "Contrato [seqContrato=" + seqContrato + ", dscContrato=" + dscContrato + ", listaAditivos="
				+ listaAditivos + ", categorias=" + categorias + "]";
	}


	
	
	
	
	
	
}
