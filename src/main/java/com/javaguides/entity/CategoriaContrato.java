package com.javaguides.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "categoria")
public class CategoriaContrato {


	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer seqCategoriaContrato;
	
	@Column(name = "dsc_categoria_contrato")
	private String dscCategoriaContrato;
	
	@ManyToMany(mappedBy = "categorias")
	private Set<Contrato> contratos;

	public Integer getSeqCategoriaContrato() {
		return seqCategoriaContrato;
	}

	public void setSeqCategoriaContrato(Integer seqCategoriaContrato) {
		this.seqCategoriaContrato = seqCategoriaContrato;
	}

	public String getDscCategoriaContrato() {
		return dscCategoriaContrato;
	}

	public void setDscCategoriaContrato(String dscCategoriaContrato) {
		this.dscCategoriaContrato = dscCategoriaContrato;
	}

	public Set<Contrato> getContratos() {
		return contratos;
	}

	public void setContratos(Set<Contrato> contratos) {
		this.contratos = contratos;
	}

	@Override
	public String toString() {
		return "dscCategoriaContrato="+ dscCategoriaContrato ;
	}
	
	
	
	
	
}
