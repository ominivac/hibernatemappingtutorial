package com.javaguides.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.javaguides.entity.Contrato;

@Repository
public interface ContratoRepo extends JpaRepository<Contrato, Integer> {
	
	String queryPesquisarContratoPorSeq = "select contrato.seq_contrato, contrato.dsc_contrato, seq_contrato_aditivo, \r\n" + 
			"dsc_aditivo, aditivo.dsc_aditivo from contrato \r\n" + 
			"left join aditivo on  contrato.seq_contrato =  aditivo.contrato_seq\r\n" + 
			"left join contrato_categoria on contrato_categoria.seq_contrato =  contrato.seq_contrato\r\n" + 
			"where contrato.seq_contrato = :seqContrato" ;
	
	
	@Query(value = queryPesquisarContratoPorSeq, nativeQuery = true)
	public Contrato pesquisarContratoPorId(@Param("seqContrato") Integer seqContrato);

}
