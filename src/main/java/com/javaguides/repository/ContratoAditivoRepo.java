package com.javaguides.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.javaguides.entity.ContratoAditivo;

@Repository
public interface ContratoAditivoRepo extends JpaRepository<ContratoAditivo, Integer> {

	
	
	
}
